
# Weavit-technical-test

- Frontend:
    - Using React with **TypeScript**
    - Package manager : **yarn**
- Backend:
    - Using Node.js with **TypeScript**
    - Package manager : **yarn**
