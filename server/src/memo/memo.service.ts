import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MemoEntity } from './memo.entity';
import { Repository } from 'typeorm';
import { MemoDTO, MemoRO } from './memo.dto';
import { UserEntity } from '../user/user.entity';

@Injectable()
export class MemoService {

    constructor(
        @InjectRepository(MemoEntity)
        private memoRepository: Repository<MemoEntity>,
        @InjectRepository(UserEntity)
        private userRepository: Repository<UserEntity>,
    ) { }

    async createMemo(userId: string, data: MemoDTO) {
        const user = await this.userRepository.findOne({ where: { id: userId } });
        const memo = await this.memoRepository.create({ ...data, user: user });

        await this.memoRepository.save(memo);
        return this.memoToResponseObject(memo);
    }

    private memoToResponseObject(memo: MemoEntity): MemoRO {
        const responseObject: any = {
            ...memo,
            user: memo.user ? memo.user.toResponseObject(false) : null,
        };
        return responseObject;
    }

    async getAllMemo(page: number = 1, newest?: boolean): Promise<MemoRO[]> {
        const memos = await this.memoRepository.find({
            relations: ['user'],
            take: 25,
            skip: 25 * (page - 1),
            order: newest && { created_at: 'DESC' },
        });
        return memos.map(memo => this.memoToResponseObject(memo));
    }

    async updateMemo(
        memoId: string,
        userId: string,
        data: Partial<MemoDTO>,
      ): Promise<MemoRO> {
        let memo = await this.memoRepository.findOne({
          where: { memoId },
          relations: ['user'],
        });
        if (!memo) {
          throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.memoRepository.update({ memoId }, data);
        memo = await this.memoRepository.findOne({
          where: { memoId },
          relations: ['user'],
        });
        return this.memoToResponseObject(memo);
      }
    
      async destroy(memoId: string, userId: string): Promise<MemoRO> {
        const memo = await this.memoRepository.findOne({
          where: { memoId },
          relations: ['user'],
        });
        if (!memo) {
          throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.memoRepository.remove(memo);
        return this.memoToResponseObject(memo);
      }


}
