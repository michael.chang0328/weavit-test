import { IsNotEmpty } from 'class-validator';
import { MemoEntity } from 'src/memo/memo.entity';

export class UserDTO {
    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    password: string;
}

export class UserRO {
    userId: string;
    username: string;
    created_at: Date;
    updated_at: Date;
    token?: string;
    memo?: MemoEntity[];
  }