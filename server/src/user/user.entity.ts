import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, BeforeInsert, OneToMany } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { MemoEntity } from 'src/memo/memo.entity';
import { UserRO } from './user.dto';

@Entity('user')
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    userId: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @Column({
        type: 'text',
        unique: true
    })
    username: string;

    @Column('text')
    password: string;

    @OneToMany(type => MemoEntity, memo => memo.userId, { cascade: true })
    memos: MemoEntity[];

    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 10);
    }

    toResponseObject(showToken: Boolean = true): UserRO {
        const { userId, created_at, updated_at, username, token } = this;
        const responseObject = { userId, created_at, updated_at, username, token };
        if (showToken) {
            responseObject.token = token;
        }
        return responseObject;
    }

    async comparePassword(attempt: string): Promise<boolean> {
        return await bcrypt.compare(attempt, this.password);
    }

    private get token(): string {
        const { userId, username } = this;

        return jwt.sign(
            {
                userId,
                username,
            },
            process.env.SECRET,
            { expiresIn: '7d' },
        );
    }
}