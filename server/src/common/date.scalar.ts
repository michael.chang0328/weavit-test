import { Scalar, CustomScalar } from '@nestjs/graphql';
import { SyntaxError } from 'apollo-server-errors';
import { Kind, ValueNode } from 'graphql';

// Date -> DateTime
@Scalar('DateTime', () => Date)
export class DateScalar implements CustomScalar<string | number, Date> {
  description = 'Date custom scalar type';

  // value from the client
  parseValue(value: string | number): Date {
    return new Date(value);
  }

  // value sent to the client
  serialize(value: number | string | Date): number {
    if (typeof value === 'string') {
      return Date.parse(value);
    }

    if (value instanceof Date) {
      return value.getTime();
    }

    if (new Date(value).getTime() > 0) {
      return value;
    }

    throw new SyntaxError(`${value} is not a valid value to serialize`);
  }

  parseLiteral(ast: ValueNode): Date | null {
    switch (ast.kind) {
      case Kind.INT:
        return new Date(Number(ast.value));

      case Kind.STRING:
        return new Date(ast.value);

      default:
        return null;
    }
  }
}